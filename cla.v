`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/25/2017 05:24:30 PM
// Design Name: 
// Module Name: cla
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module cla( g0,p0,g1,p1,g2,p2,g3,p3,cin, a, b,c0,c1,c2,c3,c4, g, p,G,P);
input g0,p0,g1,p1,g2,p2,g3,p3,cin,a, b;
output c0,c1,c2,c3,c4,g, p, G,P;

assign c1 = g0 | (p0 & c0);
assign c2 = g1 | (p1 & g0) | (p1 & p0 & c0);
assign c3 = g2 | (p2 & g1) | (p2 & p1 & g0) | (p2 & p1 & p0 & c0);
assign c4 = g3 | (p3 & g2) | (p3 & p2 & g1) | (p3 & p2 & p1 & g0) | (p3 & p2 & p1 & p0 & c0);
assign c0 = cin;
assign P = p0 & p1 & p2 & p3;
assign G = g3 | g2 & p3 | g1 & p3 & p2 | g0 & p3 & p2 & p1;
assign p = a & b;
assign g = a | b;

//implement a carry look ahead adder check it out//
endmodule
