`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/04/2017 08:58:54 AM
// Design Name: 
// Module Name: ALU32Bit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALU32Bit(a, b, op, result, zero);
    input [31:0] a, b;
    input [2:0] op;
    output [31:0] result;
    output zero;
   
   assign zero = ~(result==0);
   
   ALU16Bit alu32_1 (a[15:0], b[15:0], op, result[15:0], zero);
    ALU16Bit alu32_2 (a[31:16], b[31:16], op, result[31:16], zero);
endmodule
