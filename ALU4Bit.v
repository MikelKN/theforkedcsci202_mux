`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/27/2017 07:48:50 AM
// Design Name: 
// Module Name: ALU4Bit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALU4Bit( a, b,cin, less,op, result, cout, set, zero,g, p, G, P);
    input [3:0] a, b;
    input cin, less;
    input [2:0] op; //op[2] is "binv"
    output [3:0] result;
    output cout, set, zero, g, p, G,P;
  assign zero = ~(result[0])& ~(result[1]) & ~(result[2]) & ~(result[3]);
  assign set = result[3];
  wire p0, p1, p2, p3, g0, g1, g2, g3;
  wire c0, c1, c2, c3, c4;
  assign cin = c0; 
  assign cout = g3 | (p3 & g2) | (p3 & p2 & g1) | (p3 & p2 & p1 & g0) | (p3 & p2 & p1 & p0 & c0);
  
  ALU1Bit RESULT_1 (a[0], b[0], c0, less, op[0], op[1],set, zero, g0, p0, result[0]);
  ALU1Bit RESULT_2 (a[1], b[1], c1, less, op[0],op[1],set, zero, g1, p1, result[1]);
  ALU1Bit RESULT_3 (a[2], b[2], c2, less, op[0],op[1],set, zero, g2, p2, result[2]);
  ALU1Bit RESULT_4 (a[0], b[0], c3, less, op[0],op[1],set, zero, g3, p3, result[3]);
  cla ALADD_1 (g0,p0,g1,p1,g2,p2,g3,p3,c0, cin, c1,c2,c3,a, b,cout,g, p, G, P);
endmodule
