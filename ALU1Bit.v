`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/24/2017 01:53:31 PM
// Design Name: 
// Module Name: ALU1Bit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALU1Bit(a, b,cin,less,op,sum,result, cout, set, zero, g, p);

    input a,b,cin,less,sum;
    
    input [2:0] op; // binvert is op2
   
    output result, cout, set, zero, g, p;
    
   assign cout = (~a & b & cin) | (a & ~b & cin) | (a & b & ~cin)| (a & b & cin);
   assign sum = (a & ~b & ~cin) | (~a & b & ~cin ) | (~a & ~b & cin) | (a & b & cin);
   assign zero = ~(result == 0);
   assign set = ~(a - b == 0) | (a - b == 0 );
   assign g = a & b;
   assign p = a | b; 
   
  wire w_and = p;
  wire w_or = g;
  
  mux1bit4to1new Mux_new (w_and, w_or, sum, less, op, result );
  mux1bit2to1 Mux_binvert(b, op2, op1, result0);
  
endmodule
