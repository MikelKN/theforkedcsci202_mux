`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/04/2017 08:04:02 AM
// Design Name: 
// Module Name: ALU16Bit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALU16Bit(a, b, cin, less, op, result, cout, set, zero, G, P);
    input [15:0] a, b;
    input cin, less;
    input [2:0] op;
    output [15:0] result;
    output cout, set, zero, G, P;
    
    assign zero = ~(result==0);
    
    ALU4Bit ALU4_1B (a[3:0], b[3:0], cin, less, op, result[3:0], cout, set, zero, G, P);
    ALU4Bit ALU4_2B (a[7:4], b[7:4], cin, less, op, result[7:4], cout, set, zero, G, P);
    ALU4Bit ALU4_3B (a[11:8], b[11:8], cin, less, op, result[11:8], cout, set, zero, G, P);
    ALU4Bit ALU4_4B (a[15:12], b[15:12], cin, less, op, result[15:12], cout, set, zero, G, P);  
    
endmodule
