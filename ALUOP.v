`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/05/2017 04:37:30 PM
// Design Name: 
// Module Name: ALUOPToALUControl
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALUOPToALUControl(ALUop, Funct, ALUControl);
    input [1:0] ALUop;
    input [5:0] Funct;
    output [2:0] ALUControl;
    assign ALUop[0]= 0;
    assign ALUop[1] = 1;
endmodule
