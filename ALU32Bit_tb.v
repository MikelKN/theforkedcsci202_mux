`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/04/2017 03:33:03 PM
// Design Name: 
// Module Name: ALU32Bit_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALU32Bit_tb();
    reg [31:0] a, b;
    reg [2:0] op;
    wire [31:0] result;
    wire zero;
   
  ALU32Bit DUT (.a(a), .b(b), .op(op), .result(result), .zero(zero));
  
  initial
  
  begin
   
   a = 32'b1100_0011_1011_0100_0011_1100_0100_1011; // time = 0
   b = 32'b0011_1100_0100_1011_0011_1100_0100_1011;
   
   op = 3'b000;
   #100;
   op = 3'b001; // time = 100
   #100;
   op = 3'b010; // time = 200
   #100;
   op = 3'b110; // time = 300
   #200;
   op = 3'b111;// time = 500
   #100;
   a = 32'b1100_0011_1111_1111_1100_1111_1111_1111; //time = 600
   b = 32'b1011_0100_0011_1100_0011_0110_0011_0101;
   op = 3'b000;
      #100;
      op = 3'b001; // time = 700
      #100;
      op = 3'b010; // time = 800
      #100;
      op = 3'b110; // time = 900
      #100;
      op = 3'b111;// time = 1100
   end
  
endmodule
