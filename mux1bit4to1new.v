`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/24/2017 01:33:52 PM
// Design Name: 
// Module Name: mux1bit4to1new
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mux1bit4to1new (result0, d0,d1,d2, d3,op0,op1);
    input d0,d1,d2, d3,op0,op1;
    output result0;
    
   wire v, w;
   
   mux1bit2to1 mux1 (d0, d1, op0, v);
   mux1bit2to1 mux2 (d2, d3, op0, w);
   mux1bit2to1 mux3 (v, w, op1, result0);
endmodule //mux1bit4to1new
