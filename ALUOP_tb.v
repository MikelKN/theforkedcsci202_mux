`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/05/2017 06:06:17 PM
// Design Name: 
// Module Name: ALUOP_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALUOP_tb();
    reg [1:0] ALUop;
    reg [5:0] Funct;
    wire [2:0] ALUControl;
    
    ALUOPToALUControl DUT (.ALUop(ALUop),.Funct(Funct), .ALUControl(ALUControl));
    
    initial
    
    begin
    
     ALUop = 2'b10;
    // test add
    Funct = 6'b100000;
      #100;
      ALUop = 2'b11;
      // test sub
    Funct = 6'b100010; // time = 100
      #100;
      ALUop = 2'b00;
      // test and
    Funct = 6'b100100; // time = 200
      #100;
      ALUop = 2'b01;
      // test or
    Funct = 6'b100101; // time = 300
      #200;
      // test set less than
    Funct = 6'b101010;// time = 500
    
    end
    
endmodule
